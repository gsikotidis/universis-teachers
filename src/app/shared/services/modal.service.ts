import { Injectable, EventEmitter} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {CoursesGradesModalComponent} from '../../courses/components/courses-grades-modal/courses-grades-modal.component';


@Injectable({
  providedIn: 'root'
})

@Injectable()
export class ModalService {
  private modalRef: BsModalRef;

  public choice: string;
  config = {
    ignoreBackdropClick: true,
    keyboard: false,
    initialState: null,
    class: 'modal-content-base'
  };

  constructor(private modalService: BsModalService) {}
}
