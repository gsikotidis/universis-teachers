import { Component, OnInit, Input } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {CoursesService} from '../../services/courses.service';
import {CoursesSharedService} from '../../services/courses-shared.service';
import {ErrorService} from '../../../error/error.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
    selector: 'app-courses-details-exam',
    templateUrl: './course-details-exam.component.html',
    styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsExamComponent implements OnInit {

    public courseExam: any;
    public uploadHistory: any;
    public subDate: any;
    constructor(private _context: AngularDataContext,
                private translate: TranslateService,
                private  coursesService: CoursesService,
                private coursesSharedService: CoursesSharedService,
                private  errorService: ErrorService,
                private router: Router,
                private route: ActivatedRoute,
                private loadingService: LoadingService) {

    }

    ngOnInit(): void {
      this.loadingService.showLoading();
        this.route.params.subscribe(routeParams => {
            this.coursesService.getCourseExam(routeParams.courseExam).then((result) => {
                this.courseExam = result;
              this.coursesService.getUploadHistory(this.courseExam.id).then((value) => {
                this.uploadHistory = value.value;
                if (this.uploadHistory && this.uploadHistory.length > 0) {

                  this.subDate = this.uploadHistory[0].result.dateCreated;
                }
                this.loadingService.hideLoading();
              }).catch(err => {
                return this.errorService.navigateToError(err);
              });
            }).catch(err => {
                this.loadingService.hideLoading();
                return this.errorService.navigateToError(err);
            });
        });
    }

    importGrades(courseExam) {
        return this.coursesSharedService.importCourseExamGradesDialog(courseExam);
    }

    exportGrades(courseExam) {
        const headers = new Headers();
        const serviceHeaders = this._context.getService().getHeaders();
        Object.keys(serviceHeaders).forEach((key) => {
            if (serviceHeaders.hasOwnProperty(key)) {
                headers.set(key, serviceHeaders[key]);
            }
        });
        const fileURL = this._context.getService().resolve(`instructors/me/exams/${courseExam.id}/students/export`);
        fetch(fileURL, {
            headers: headers,
            credentials: 'include'
        }).then((response) => {

            return response.blob();
        })
            .then(blob => {
                const objectUrl = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                document.body.appendChild(a);
                a.setAttribute('style', 'display: none');
                a.href = objectUrl;
                const name = courseExam.name.replace(/[/\\?%*:|"<>]/g, '-');
                a.download = `${name}-${courseExam.year.id}-${courseExam.examPeriod.name}.xlsx`;
                a.click();
                window.URL.revokeObjectURL(objectUrl);
                a.remove(); // remove the element
            });
    }

}
